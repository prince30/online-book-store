package com.desh.bookstore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.desh.bookstore.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

	User findByEmail(String email);
	
	User findByUsername(String username);
}
