package com.desh.bookstore.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.desh.bookstore.model.Category;

public interface CategoryRepository extends JpaRepository<Category, Long> {

}
