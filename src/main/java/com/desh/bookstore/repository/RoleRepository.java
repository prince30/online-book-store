package com.desh.bookstore.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.desh.bookstore.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {

}
