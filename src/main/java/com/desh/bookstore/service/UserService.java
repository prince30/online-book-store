package com.desh.bookstore.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.desh.bookstore.dto.UserRegistrationRequest;
import com.desh.bookstore.model.User;

public interface UserService extends UserDetailsService {

	User save(UserRegistrationRequest userRegistrationRequest);
}
