package com.desh.bookstore.service;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import com.desh.bookstore.dto.UserRegistrationRequest;
import com.desh.bookstore.model.User;
import com.desh.bookstore.repository.UserRepository;
import com.desh.bookstore.model.Role;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByUsername(username);

		if (user == null) {
			throw new UsernameNotFoundException("Invalid username and password");
		}

		return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(),
				mapRolesToAuthorities(user.getRoles()));
	}

	private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Role> roles) {
		return roles.stream().map(role -> new SimpleGrantedAuthority(role.getName())).collect(Collectors.toList());
	}

	@Override
	public User save(UserRegistrationRequest userRegistrationRequest) {

		User user = new User(userRegistrationRequest.getFirstName(), userRegistrationRequest.getLastName(),
				userRegistrationRequest.getUsername(), userRegistrationRequest.getEmail(),
				userRegistrationRequest.getPhone(), passwordEncoder.encode(userRegistrationRequest.getPassword()),
				Arrays.asList(new Role("ROLE_USER")));

		return userRepository.save(user);
	}
}
