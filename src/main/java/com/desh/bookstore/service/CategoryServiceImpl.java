package com.desh.bookstore.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.desh.bookstore.dto.CategoryRequest;
import com.desh.bookstore.model.Category;
import com.desh.bookstore.repository.CategoryRepository;

@Service
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	private CategoryRepository categoryRepository;

	@Override
	public Category save(CategoryRequest categoryRequest) {

		Category category = new Category(categoryRequest.getName());

		return categoryRepository.save(category);
	}

	@Override
	public Category update(Category category) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(Category category) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Category getById(Long Id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Category> getAll() {
		return categoryRepository.findAll();
	}

}
