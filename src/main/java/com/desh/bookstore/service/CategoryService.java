package com.desh.bookstore.service;

import java.util.List;

import com.desh.bookstore.dto.CategoryRequest;
import com.desh.bookstore.model.Category;

public interface CategoryService {

	Category save(CategoryRequest categoryRequest);

	Category update(Category category);

	void delete(Category category);

	Category getById(Long Id);

	List<Category> getAll();
}
