package com.desh.bookstore.dto;

public class CategoryRequest {

	private String name;

	public CategoryRequest() {
		super();
	}

	public CategoryRequest(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
