package com.desh.bookstore.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.desh.bookstore.dto.AuthRequest;
import com.desh.bookstore.util.JwtUtil;

@Controller
public class AdminController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtUtil jwtUtil;

	private static final Logger LOGGER = LoggerFactory.getLogger(AdminController.class);

	@RequestMapping(value = "/admin-login", method = RequestMethod.GET)
	public String login() {
		LOGGER.info("Home Page");
		return "admin/login/login";
	}

	@RequestMapping(value = "/admin", method = RequestMethod.GET)
	public String index() {
		LOGGER.info("Home Page");
		return "admin/dashboard/dashboard";
	}

	@RequestMapping(value = "/admin-login", method = RequestMethod.POST)
	public String generateToken(AuthRequest authRequest) throws Exception {
		try {
			authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword()));
		} catch (Exception e) {
			throw new Exception("Invalid username and password");
		}

		String token = jwtUtil.generateToken(authRequest.getUsername());

		LOGGER.info("jwt token :" + token);

		if (token != "") {
			return "admin/dashboard/dashboard";
		}
		return "admin/login/login";
	}
}
