package com.desh.bookstore.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.desh.bookstore.dto.CategoryRequest;
import com.desh.bookstore.model.Category;
import com.desh.bookstore.service.CategoryService;

@Controller
public class CategoryController {

	@Autowired
	private CategoryService categoryService;

	private static final Logger LOGGER = LoggerFactory.getLogger(CategoryController.class);

	@RequestMapping(value = "/category-list", method = RequestMethod.GET)
	public String index(Model model) {
		List<Category> categories = categoryService.getAll();
		model.addAttribute("categories", categories);
		return "admin/category/index";
	}

	@RequestMapping(value = "/category-add", method = RequestMethod.GET)
	public String create() {
		LOGGER.info("Home Page");
		return "admin/category/create";
	}

	@RequestMapping(value = "/category-add", method = RequestMethod.POST)
	public String store(CategoryRequest categoryRequest) {
		categoryService.save(categoryRequest);
		LOGGER.info("Home Page");
		return "admin/category/index";
	}

}
