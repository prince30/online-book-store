package com.desh.bookstore.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.desh.bookstore.dto.AuthRequest;
import com.desh.bookstore.dto.UserRegistrationRequest;
import com.desh.bookstore.service.UserService;
import com.desh.bookstore.util.JwtUtil;

@Controller
public class UserController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtUtil jwtUtil;

	@Autowired
	private UserService userService;

	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login() {
		LOGGER.info("Login Page");
		return "front/users/login";
	}

	@RequestMapping(value = "/registration", method = RequestMethod.GET)
	public String create() {
		LOGGER.info("Registration Page");
		return "front/users/registration";
	}

	@RequestMapping(value = "/store", method = RequestMethod.POST)
	public String store(UserRegistrationRequest userRegistrationRequest) {
		userService.save(userRegistrationRequest);
		return "front/users/login";

	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String generateToken(AuthRequest authRequest) throws Exception {
		try {
			authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword()));
		} catch (Exception e) {
			throw new Exception("Invalid username and password");
		}

		String token = jwtUtil.generateToken(authRequest.getUsername());

		LOGGER.info("jwt token :" + token);

		if (token != "") {
			return "front/users/success";
		}
		return "front/users/login";
	}
}
